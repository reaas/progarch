package com.core;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.core.apis.FirebaseServices;
import com.core.game.MainGame;
import com.core.game.controllers.EventManager;
import com.core.game.models.entities.player.Opponent;
import com.core.game.models.entities.player.Player;
import com.core.objects.Match;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class AndroidLauncher extends AndroidApplication implements FirebaseServices {
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private FirebaseAuth mAuth;
    private FirebasePlayerMatchMaker matchMaker;

    private static MainGame mainGame;

    private boolean opponentShot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.database = FirebaseDatabase.getInstance();
        this.mAuth = FirebaseAuth.getInstance();

        mainGame = new MainGame(this);

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(mainGame, config);
    }

    /*
     * Handles the registration process with Firebase Authentication
     */
    @Override
    public void onRegisterButtonClicked(String email, String password) {
        FirebaseUser currentUser = mAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            // User is signed in
            Log.d("onSignUpButtonClicked", "User already signed in");
        } else {
            // No user is signed in
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d("onSignUpButtonClicked", "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                Toast.makeText(AndroidLauncher.this, "Signed in.",
                                        Toast.LENGTH_SHORT).show();
                                // upload standard castle data
                                Map<String, String> userData = new HashMap<>();
                                userData.put("Balance", Integer.toString(1000));
                                userData.put("bthp", Integer.toString(20));
                                userData.put("fthp", Integer.toString(20));
                                userData.put("rhp", Integer.toString(20));
                                EventManager.getInstance().updateUserData(userData);
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("onSignUpButtonClicked", "createUserWithEmail:failure", task.getException());
                                Toast.makeText(AndroidLauncher.this, "Registration failed.",
                                        Toast.LENGTH_SHORT).show();
                                // updateUI/screen;
                            }

                            // ...
                        }
                    });
        }
    }

    /*
     * Handles the login process with Firebase Authentication
     */
    @Override
    public void onLogInButtonClicked(String email, String password) {
        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        if (currentUser != null) {
            // User is signed in
            Log.d("onLogInButtonClicked", "User already signed in");
        } else {
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(
                    this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Log in success, update screen
                                Log.d("onSignUpButtonClicked", "signInWithEmailAndPassword:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                            } else {
                                // If Log in fails show feedback to user. update screen
                                Log.w("onSignUpButtonClicked", "signInWithEmailAndPassword:failure",
                                        task.getException());
                                Toast.makeText(AndroidLauncher.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    /*
     * Handles the logout process with Firebase Authentication
     */
    @Override
    public void onLogOutButtonClicked() {
        mAuth.signOut();
    }

    /*
     * When the users wishes to play a game, the match making process starts. The match maker is
     * taken from https://medium.com/free-code-camp/match-making-with-firebase-hashnode-de9161e2b6a7.
     *
     * It works like this:
     *      -   If there's no matches listed in the "matches" collection in the Firebase database,
     *          the process creates a new document and waits for someone to look for a match.
     *      -   If there is a document in the "matches" collection, start the pairing process.
     *      -   After the pairing process is complete, the document is discarded and a new document
     *          is created in the "OpenGameMoves" collection. Both devices that were paired get the
     *          same database reference such that data can be shared between them.
     */
    @Override
    public void onPlayButtonClicked() {
        matchMaker = FirebasePlayerMatchMaker.newInstance("/matches", new StartMatch());
        matchMaker.findMatch();
    }

    /*
     * Called after a match is found
     */
    private class StartMatch implements FirebasePlayerMatchMaker.OnMatchMadeCallback {
        @Override
        public void run(FirebasePlayerMatchMaker c) {
            FirebaseDatabase.getInstance().getReference().child(c.mGamePath).addValueEventListener(new ValueEventListener() {
                /*
                 * Ran each time the data in the match-document is changes, e.g. game state change
                 */
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    Match tempMatch = dataSnapshot.getValue(Match.class);

                    // First instance of match is null as this is set just before the match starts
                    if (tempMatch != null && !tempMatch.isInit) {
                        if (Player.getInstance().isOpener) {
                            /*
                             * If the player is player1 in the cloud
                             */
                            Player.getInstance().waiting = !tempMatch.player1.playing;
                            for (int i = 0; i < Player.getInstance().getCastle().getComponents().size(); i++) {
                                Player.getInstance().getCastle().getComponents().get(i).setHealth(tempMatch.player1.castle.components.get(i).health);
                            }
                            for (int i = 0; i < Opponent.getInstance().getCastle().getComponents().size(); i++) {
                                Opponent.getInstance().getCastle().getComponents().get(i).setHealth(tempMatch.player2.castle.components.get(i).health);
                            }

                            if (!tempMatch.player2.isAlive) {
                                Opponent.getInstance().getCastle().getComponents().get(0).setHealth(0);
                            }
                        } else {
                            /*
                             * If the player is player2 in the cloud
                             */
                            Player.getInstance().waiting = !tempMatch.player2.playing;
                            for (int i = 0; i < Player.getInstance().getCastle().getComponents().size(); i++) {
                                Player.getInstance().getCastle().getComponents().get(i).setHealth(tempMatch.player2.castle.components.get(i).health);
                            }
                            for (int i = 0; i < Opponent.getInstance().getCastle().getComponents().size(); i++) {
                                Opponent.getInstance().getCastle().getComponents().get(i).setHealth(tempMatch.player1.castle.components.get(i).health);
                            }

                            if (!tempMatch.player1.isAlive) {
                                Opponent.getInstance().getCastle().getComponents().get(0).setHealth(0);
                            }
                        }

                        if (!Player.getInstance().waiting) {
                            /*
                             * If the current player is next, fire the last shot from the opponent,
                             * but do not send the data to the cloud. Only for show as damage is
                             * already calculated.
                             */
                            Opponent.getInstance().getCannon().getCannon().setAngle(tempMatch.projectile.angle);
                            Opponent.getInstance().getCannon().getCannon().setPower(tempMatch.projectile.power);

                            opponentShot = true;

                            EventManager.getInstance().opponentFire(
                                    Opponent.getInstance().getCannon().getCannon(),
                                    Opponent.getInstance().getBall().getProjectile());
                        }
                    } else if (tempMatch != null && tempMatch.isInit) {
                        /*
                         * Handles the initial match setup. Sets the initial game state
                         */
                        if (Player.getInstance().isOpener) {
                            if (tempMatch.player1 != null) {
                                for (int i = 0; i < Player.getInstance().getCastle().getComponents().size(); i++) {
                                    Player.getInstance().getCastle().getComponents().get(i).setHealth(tempMatch.player1.castle.components.get(i).health);
                                }
                            }
                            if (tempMatch.player2 != null) {
                                for (int i = 0; i < Opponent.getInstance().getCastle().getComponents().size(); i++) {
                                    Opponent.getInstance().getCastle().getComponents().get(i).setHealth(tempMatch.player2.castle.components.get(i).health);
                                }
                            }
                        } else {
                            if (tempMatch.player1 != null) {
                                for (int i = 0; i < Opponent.getInstance().getCastle().getComponents().size(); i ++) {
                                    Opponent.getInstance().getCastle().getComponents().get(i).setHealth(tempMatch.player1.castle.components.get(i).health);
                                }
                            }
                            if (tempMatch.player2 != null) {
                                for (int i = 0; i < Player.getInstance().getCastle().getComponents().size(); i++) {
                                    Player.getInstance().getCastle().getComponents().get(i).setHealth(tempMatch.player2.castle.components.get(i).health);
                                }
                            }
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    // TODO Implement error handling
                }
            });

            // Start the initial match building. mIsthisOpener determines which player goes
            // first
            EventManager.getInstance().startMatch(c.mIsThisOpener, c.mGamePath);
        }
    }

    /*
     * Sending the current game state to the Firebase Database, such that all other connected
     * devices are updated.
     */
    @Override
    public void onUpdateMatch(Match match) {
        if (match != null) {
            if (match.isInit) {
                // Update the database with the initial values of the player
                FirebaseDatabase.getInstance().getReference().child(Player.gameRef).setValue(match);
            } else if (!opponentShot) {
                // Update the database after the player has fired a shot
                FirebaseDatabase.getInstance().getReference().child(Player.gameRef).setValue(match);
            } else {
                // Only shown the last shot from opponent. The database should not be updated as
                // it already has the newest data.
                opponentShot = false;
            }
        }
    }

    /*
     * Updates the user data in the Firebase database.
     */
    @Override
    public void onUpdateUserData(Map userData) {
        if(mAuth.getCurrentUser() != null) {
            FirebaseDatabase.getInstance().getReference().child("UserData").child(
                    mAuth.getCurrentUser().getUid()).setValue(userData);
        }
    }

    /*
     * Called when logging in. Gets the stored game state (castle upgrades) from Firebase, and
     * sets the values on the Player instance
     */
    public void onGetUserData() {
        FirebaseDatabase.getInstance().getReference().child("UserData").child(
                mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Map<String, String> userData = new HashMap<>();
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    userData.put(ds.getKey(), ds.getValue(String.class));
                }
                if(userData.get("Balance") != null) {
                    Player.getInstance().setBalance(Integer.parseInt(userData.get("Balance")));
                } else {
                    Player.getInstance().setBalance(1000);
                }
                if(userData.get("bthp") != null) {
                    Player.getInstance().setBthp(Integer.parseInt(userData.get("bthp")));
                } else {
                    Player.getInstance().setBthp(20);
                }
                if(userData.get("fthp") != null) {
                    Player.getInstance().setFthp(Integer.parseInt(userData.get("fthp")));
                } else {
                    Player.getInstance().setFthp(20);
                }
                if(userData.get("rhp") != null) {
                    Player.getInstance().setRhp(Integer.parseInt(userData.get("rhp")));
                } else {
                    Player.getInstance().setRhp(20);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // TODO Implement error handling
            }
        });
    }


    @Override
    public void onUserDelete() {
        if(mAuth.getCurrentUser() != null) {
            mAuth.getCurrentUser().delete();
        }
    }

    @Override
    public void onMatchCancel() {
        matchMaker.stop();
    }

    // DELETE?
    @Override
    public void onMatchFound() {
    }

    @Override
    public boolean isSignedIn() { // true == signed in, //false == not signed in
        FirebaseUser currentUser = this.mAuth.getCurrentUser();
        return currentUser != null; // true if logged in, false if not logged in
    }
}