package com.core.objects;

import com.core.game.models.components.castlecomponents.CastleComponent;
import com.core.game.models.entities.Castle;

import java.util.ArrayList;

public class MatchCastle {
    /*
     * All public variables will be sent and received by Firebase
     */
    public ArrayList<MatchCastleComponent> components;
    public ArrayList<MatchCastleComponent> removedComponents; // Maybe safe to delete

    /*
     * Empty constructor such that Firebase can build the object correctly on receiving
     * of game state
     */
    public MatchCastle() {}

    /*
     * Constructing the castle with components for the player
     */
    public MatchCastle(Castle castle) {
        this.components = new ArrayList<>();
        this.removedComponents = new ArrayList<>(); // Maybe safe to delete

        for (CastleComponent component : castle.getComponents()) {
            this.components.add(new MatchCastleComponent(component));
        }

        /*
         * Maybe safe to delete
         */
        for (CastleComponent component : castle.getDeadComponents()) {
            this.removedComponents.add(new MatchCastleComponent(component));
        }
    }
}
