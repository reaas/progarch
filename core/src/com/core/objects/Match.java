package com.core.objects;

import com.core.game.models.entities.player.Opponent;
import com.core.game.models.entities.player.Player;

public class Match {
    /*
     * All public variables will be sent and received by Firebase
     */
    public MatchPlayer player1;
    public MatchPlayer player2;
    public MatchProjectile projectile = new MatchProjectile();
    public boolean isInit;

    // The Firebase database reference of the current match
    private String gameRef;

    /*
     * Empty constructor such that Firebase can build the object correctly on receiving
     * of game state
     */
    public Match() {}

    /*
     * Setting the game state correctly according to which player is the first one to
     * press "play"
     */
    public Match(Player player1, boolean isOpener) {
        if (isOpener) {
            this.player1 = new MatchPlayer(player1);
            this.player1.playing = true;
        } else {
            this.player2 = new MatchPlayer(player1);
            this.player2.playing = false;
        }

        this.isInit = true;

        this.projectile = new MatchProjectile();
    }

    /*
     * "Safe delete".... but is it though?!
     */
    public Match(Player player1, Opponent player2) {
        this.player1 = new MatchPlayer(player1);
        this.player2 = new MatchPlayer(player2);

        this.isInit = false;

        this.projectile = new MatchProjectile();
    }

    /*
     * "Safe delete".... but is it though?!
     */
    public Match(Opponent player1, Player player2) {
        this.player1 = new MatchPlayer(player1);
        this.player2 = new MatchPlayer(player2);

        this.isInit = false;

        this.projectile = new MatchProjectile();
    }

    /*
     * "Safe delete".... but is it though?!
     */
    public void updateMatch(Player player1, boolean isOpener) {
        if (isOpener) {
            this.player1 = new MatchPlayer(player1);
        } else {
            this.player2 = new MatchPlayer(player1);
        }

        this.isInit = false;
    }

    /*
     * Updates the match from the first players side
     */
    public void updateMatch(Player player1, Opponent player2) {
        this.player1 = new MatchPlayer(player1);
        this.player2 = new MatchPlayer(player2);
        this.isInit = false;
    }

    /*
     * Updates the match from the second players side
     */
    public void updateMatch(Opponent player1, Player player2) {
        this.player1 = new MatchPlayer(player1);
        this.player2 = new MatchPlayer(player2);
        this.isInit = false;
    }

    /*
     * "Safe delete".... but is it though?!
     */
    public void setGameRef(String ref) {
        this.gameRef = ref;
    }

    /*
     * "Safe delete".... but is it though?!
     */
    public String getGameRef() {
        return this.gameRef;
    }
}
