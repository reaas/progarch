package com.core.objects;

import com.core.game.models.components.castlecomponents.CastleComponent;

public class MatchCastleComponent {
    /*
     * All public variables will be sent and received by Firebase
     */
    public int health;

    /*
     * Empty constructor such that Firebase can build the object correctly on receiving
     * of game state
     */
    public MatchCastleComponent() {}

    /*
     * Setting the health for the castle component such that it is ready to be sent
     * to Firebase
     */
    public MatchCastleComponent(CastleComponent component) {
        this.health = component.getHealth();
    }
}
