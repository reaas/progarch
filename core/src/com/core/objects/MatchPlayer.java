package com.core.objects;

import com.core.game.models.entities.player.Opponent;
import com.core.game.models.entities.player.Player;

public class MatchPlayer {
    /*
     * All public variables will be sent and received by Firebase
     */
    public MatchCastle castle;
    public boolean playing;
    public boolean isAlive = true;

    /*
     * Empty constructor such that Firebase can build the object correctly on receiving
     * of game state
     */
    public MatchPlayer() {}


    /*
     * If the current player is player1 in the cloud
     */
    public MatchPlayer(Player player) {
        castle = new MatchCastle(player.getCastle());
    }

    /*
     * If the current player is player2 in the cloud
     */
    public MatchPlayer(Opponent player) {
        castle = new MatchCastle(player.getCastle());
    }
}
