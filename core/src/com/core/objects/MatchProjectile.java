package com.core.objects;

public class MatchProjectile {
    /*
     * All public variables will be sent and received by Firebase
     */
    public float angle = 0;
    public int power = 0;

    /*
     * Empty constructor such that Firebase can build the object correctly on receiving
     * of game state
     */
    public MatchProjectile() {}
}
