package com.core.game.models;


public class Timer {

    //singleton pattern is used here

    private static final Timer INSTANCE = new Timer();

    private Timer() {}

    public static Timer getInstance() {
        return INSTANCE;
    }

    private float currentTime;
    private float startTime;
    private boolean timeOut;

    public void Intit(float time){
        startTime=time;
        currentTime=time;
    }

    public float getStartTime() {
        return startTime;
    }

    public float getTime() {
        return currentTime;
    }

    public void updateTime(float time) {
        this.currentTime -= time;
        if(this.currentTime<= 0){
            timeOut=true;
            this.currentTime = this.startTime;
        }

    }

    public boolean isTimeOut() {
        return timeOut;
    }

    public void reset(){
        this.currentTime = this.startTime;
        timeOut = false;
    }

}
