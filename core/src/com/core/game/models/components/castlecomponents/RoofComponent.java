package com.core.game.models.components.castlecomponents;

import com.badlogic.gdx.math.Rectangle;
import com.core.game.models.components.Projectile;
import com.core.game.models.components.castlecomponents.CastleComponent;

public class RoofComponent extends CastleComponent {

    private int health;

    private int width, height, pixelWidth, pixelHeight;
    private int x, y, xOffset, yOffset;



    private boolean removable = false;

    public RoofComponent(){}


    public void Init(int x, int y, int health){
        this.x = x;
        this.y = y;
        this.xOffset = 94;
        this.yOffset = 175;
        this.pixelWidth = 262;
        this.pixelHeight = 60;
        this.width = 450;
        this.height = 270;
        this.health = health;


    }
    public int getx() {
        return x;
    }

    public void setx(int x) {
        this.x = x;
    }

    public int gety() {
        return y;
    }

    public void sety(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getPixelWidth() {
        return pixelWidth;
    }

    public void setPixelWidth(int pixelWidth) {
        this.pixelWidth = pixelWidth;
    }

    public int getPixelHeight() {
        return pixelHeight;
    }

    public void setPixelHeight(int pixelHeight) {
        this.pixelHeight = pixelHeight;
    }

    public int getxOffset() {
        return xOffset;
    }

    public void setxOffset(int xOffset) {
        this.xOffset = xOffset;
    }

    public int getyOffset() {
        return yOffset;
    }

    public void setyOffset(int yOffset) {
        this.yOffset = yOffset;
    }

    /**
     *
     * @return The bounding rectangle of the CastleComponent.
     */
    public Rectangle getBoundingRect(){
        return new Rectangle(x+xOffset, y+yOffset, pixelWidth, pixelHeight);
    }
    /**
     *
     * @return Is the CastleComponent removeable.
     */
    public boolean isRemovable() {
        return removable;
    }

    /**
     *
     * @param removable
     */
    public void setRemovable(boolean removable) {
        this.removable = removable;
    }

    /**
     *
     * @return True: if health > 0 ; False: if health == (<=) 0.
     */
    public boolean isAlive() {
        return Double.compare(health, 0) > 0;
    }


    /**
     *
     * @param projectile
     * @return Whether or not the CastleComponent was hit by the projectile.
     */
    public boolean hitBy(Projectile projectile) {
        return getBoundingRect().overlaps(projectile.getBoundingRect());
    }


    /**
     * Reduce health by the amount of damage that the given sprite can inflict
     * minimum fixed to 0
     * @param projectile
     */
    public void getDamagedBy(Projectile projectile) {
        health -= projectile.getDamage();

        if(getHealth() <= 0) {
            kill();
        }
    }

    /**
     * Set health to 0
     */
    public void kill() {
        setHealth(0);
        remove();
    }

    /**
     * Set flag that the sprite can be removed from the UI.
     */
    public void remove() {
        setRemovable(true);
    }

}
