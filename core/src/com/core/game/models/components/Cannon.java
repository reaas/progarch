package com.core.game.models.components;

import com.core.game.controllers.PhysicsController;
import com.badlogic.gdx.math.Rectangle;

public class Cannon {

    private float angle;
    private float power;
    private int x, y;
    private int width, heigth;
    private double rotation;
    private boolean fired;
    private PhysicsController physics = PhysicsController.getInstance();


    public Cannon(){};

    public void Init(int x, int y, int width, int heigth, float power) {
        this.x = x;
        this.y = y;
        this.power = power;
        this.width = width;
        this.heigth = heigth;
        this.setFired(false);
    }




    public PhysicsController getPhysics() {
        return physics;
    }



    public void setFired(boolean fired) {
        this.fired = fired;
    }

    public boolean isFired(){
        return this.fired;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public float getAngle() {
        return angle;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public float getPower() {
        return power;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }

    public void setHeight(int height) {
        this.heigth = height;
    }

    public int getHeight() {
        return heigth;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public double getRotation() {
        return rotation;
    }

    public Rectangle getBoundingRect(){
        return new Rectangle(this.x, this.y, this.width, this.heigth);
    }

    public void shoot(Projectile projectile){
        getPhysics().decomposeSpeed(this.power, (float)Math.toRadians(this.angle), projectile);
        this.setFired(true);
    }

}
