package com.core.game.models.components;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class Explosion {

    public static final float FRAME_LENGTH = 0.2f;
    public static final int SIZE = 64;


    private static Animation<TextureRegion> anim = null;
    private float x, y;
    private float statetime;

    public boolean remove = false;

    public Explosion (float x, float y){
        this.x = x;
        this.y = y;
        statetime = 0;

        if (anim == null) {
            anim = new Animation<>(FRAME_LENGTH, TextureRegion.split(new Texture("castle/Explosion-sprite-sheet.png"), SIZE, SIZE)[0]);
        }
    }


    public void update (float deltatime){
        statetime += deltatime;
        if (anim.isAnimationFinished(statetime)){
            remove = true;
        }

    }

    public void render (SpriteBatch batch){
        batch.begin();
        batch.draw(anim.getKeyFrame(statetime), x ,y);
        batch.end();
    }

}
