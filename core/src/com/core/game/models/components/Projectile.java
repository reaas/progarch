package com.core.game.models.components;

import com.badlogic.gdx.math.Rectangle;
import com.core.game.controllers.EventManager;
import com.core.game.controllers.PhysicsController;
import com.core.game.models.entities.player.Opponent;
import com.core.game.models.entities.player.Player;

public class Projectile {

    private float speedX;
    private float speedY;
    private float x, y;
    private int width, height;
    private PhysicsController physics = PhysicsController.getInstance();

    //amount of damage dealt by the projectile
    private double damage;

    public Projectile(){}

    public void Init(float x, float y, int width, int height, double damage) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.damage = damage;
    }

    public PhysicsController getPhysics() {
        return physics;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getSpeedY(){
        return speedY;
    }

    public void setSpeedY(float speed) {
        this.speedY = speed;
    }

    public float getSpeedX(){
        return speedX;
    }

    public void setSpeedX(float speedX) {
        this.speedX = speedX;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getDamage() {
        return damage;
    }

    public Rectangle getBoundingRect(){
        return new Rectangle(this.x, this.y, this.width, this.height);
    }

    public boolean isMoving(){
        return (this.speedX != 0 && this.speedY != 0);
    }

    public void move(boolean fired){
        if(fired){getPhysics().calculateNextPosition(this);}
    }

    public void stop(){
        if (this.isMoving()){
            this.speedX = 0; this.speedY = 0;
        }
    }


    public void reset(float x, float y){
        setX(x);
        setY(y);
        stop();

        EventManager.getInstance().updateMatch();
    }


}
