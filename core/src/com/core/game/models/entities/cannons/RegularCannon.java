package com.core.game.models.entities.cannons;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.core.game.models.components.Cannon;

public class RegularCannon {

    private Sprite cannonSprite;
    private Texture badCannon;
    private SpriteBatch batch;
    public BitmapFont font;
    private Cannon cannon;

    public RegularCannon(){cannon = new Cannon();}

    public void Init(int x, int y, float power) {
        this.badCannon = new Texture("cannon.PNG");
        this.cannonSprite = new Sprite(badCannon);
        this.batch = new SpriteBatch();
        this.font = new BitmapFont();
        cannon.Init(x, y, badCannon.getWidth(), badCannon.getHeight(), power);
    }

    public SpriteBatch getBatch(){
        return this.batch;
    }

    public Cannon getCannon(){
        return this.cannon;
    }

    public void render(){
        batch.begin();
        batch.draw(cannonSprite, this.getCannon().getX(),this.getCannon().getY(),this.getCannon().getWidth()/2,this.getCannon().getHeight()/2,
                this.getCannon().getWidth(),this.getCannon().getHeight(),1,1,this.getCannon().getAngle());
        batch.end();
    }

    public void dispose(){
        badCannon.dispose();
        batch.dispose();
    }

}
