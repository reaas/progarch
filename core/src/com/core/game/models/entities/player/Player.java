package com.core.game.models.entities.player;

import com.badlogic.gdx.Gdx;
import com.core.game.MainGame;
import com.core.game.models.entities.cannons.RegularCannon;
import com.core.game.models.entities.projectiletypes.RegularProjectile;
import com.core.game.controllers.EventManager;
import com.core.game.models.entities.Castle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/*
 * The Player class is one of the main classes in this game, as it handles all functionality of the
 * player. It uses the Singleton Pattern since there will always be only one Player for each instance.
 *
 * The Player data is gathered from the Firebase database at start up, and each castle component is
 * updated accordingly.
 *
 * More information on the differences between the Player and the Opponent is found in the
 * Opponent class
 */
public class Player {

    private int balance;
    private int bthp, fthp, rhp;
    private ArrayList<Integer> prices;

    public static boolean soundOn = false;

    public static int MAX_HEALTH = 500;

    public static String gameRef;

    public boolean waiting;
    public boolean isOpener;

    private Castle castle;
    private RegularCannon cannon;
    private RegularProjectile ball;

    private static final Player instance = new Player();

    public Player(){}

    public static Player getInstance(){
        return instance;
    }

    public void Init(){
        this.castle = new Castle();
        castle.Init(Gdx.graphics.getWidth()/2-905, 40, true);
        this.cannon = new RegularCannon();
        cannon.Init(castle.getX()+(castle.getWidth()/2)-56, this.castle.getY()+250, 1f);
        cannon.getCannon().setAngle(1);
        this.ball = new RegularProjectile();
        ball.Init(this.cannon.getCannon().getX()+this.cannon.getCannon().getWidth()/2,this.cannon.getCannon().getY()+this.cannon.getCannon().getHeight()/2,20,20,10);

        this.balance = 1000;
        this.bthp = castle.getComponents().get(1).getHealth();
        this.rhp = castle.getComponents().get(2).getHealth();
        this.fthp = castle.getComponents().get(3).getHealth();
        this.prices = new ArrayList<>();
        prices.add(this.bthp);
        prices.add(this.rhp);
        prices.add(this.fthp);
    }

    public int getValue() {
        return this.fthp + this.rhp + this.bthp;
    }

    public void save() {
        Map<String, String> userData = new HashMap<String, String>();
        userData.put("Balance", String.valueOf(balance));
        userData.put("bthp", String.valueOf(bthp));
        userData.put("fthp", String.valueOf(fthp));
        userData.put("rhp", String.valueOf(rhp));

        EventManager.getInstance().updateUserData(userData);
        EventManager.getInstance().playSound(MainGame.sounds.get(4));
    }

    public Castle getCastle() {
        return castle;
    }

    public void setCastle(Castle castle) {
        this.castle = castle;
    }

    public RegularCannon getCannon() {
        return cannon;
    }

    public void setCannon(RegularCannon cannon) {
        this.cannon = cannon;
    }

    public RegularProjectile getBall() {
        return ball;
    }

    public void setBall(RegularProjectile ball) {
        this.ball = ball;
    }

    public static boolean isSoundOn() {
        return soundOn;
    }

    public static void setSoundOn(boolean soundOn) {
        Player.soundOn = soundOn;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getBthp() {
        return bthp;
    }

    public void setBthp(int bthp) {
        this.bthp = bthp;
        this.castle.getComponents().get(1).setHealth(bthp);
    }

    public int getRhp() {
        return rhp;
    }

    public void setRhp(int rhp) {
        this.rhp = rhp;
        this.castle.getComponents().get(2).setHealth(rhp);
    }

    public int getFthp() {
        return fthp;
    }

    public void setFthp(int fthp) {
        this.fthp = fthp;
        this.castle.getComponents().get(3).setHealth(fthp);
    }



    public void setPrice(int i, int price){
        this.prices.remove(i);
        this.prices.add(i, price);
    }

    public ArrayList<Integer> getPrices(){
        return this.prices;
    }

    //Helper method to reset the position and speed of the projectile.
    public void endTurn(){
        this.ball.getProjectile().reset(this.cannon.getCannon().getX()+this.cannon.getCannon().getWidth()/2,this.cannon.getCannon().getY()+this.cannon.getCannon().getHeight()/2);
        this.cannon.getCannon().setFired(false);
    }
}
