package com.core.game.models.entities.player;

import com.badlogic.gdx.Gdx;
import com.core.game.models.entities.cannons.RegularCannon;
import com.core.game.models.entities.projectiletypes.RegularProjectile;
import com.core.game.models.entities.Castle;

/*
 * The Opponent class handles the logic for the castle on the right side of the screen.
 * This will be the other devices castle. Since there will always be only one opponent at a time,
 * the group decided that it should use the Singleton Pattern. Hence the static final instance field.
 *
 * The class is similar to the Player class, but the Opponent class does not do any damage as the
 * damage is handled by the player who fires the shot. This can be prone to cheating attempts, but
 * the group did not have security as a quality attribute.
 * Since the Opponent is mirrored in layout to the Player, the group decided that they should be
 * two distinct classes. This also defines the Singleton Pattern.
 */
public class Opponent {

    private Castle castle;
    private RegularCannon cannon;
    private RegularProjectile ball;


    private static final Opponent instance = new Opponent();

    public boolean waiting;

    public Opponent(){}

    public static Opponent getInstance() {
        return instance;
    }

    public void Init(){
        this.castle = new Castle();
        castle.Init((Gdx.graphics.getWidth()/2+455), 40, false);
        this.cannon = new RegularCannon();
        cannon.Init(castle.getX()+(castle.getWidth()/2)-56, this.castle.getY()+250, 1.0f);
        this.ball = new RegularProjectile();
        ball.Init(this.cannon.getCannon().getX()+this.cannon.getCannon().getWidth()/2,this.cannon.getCannon().getY()+this.cannon.getCannon().getHeight()/2,20,20,10);

        //cannon.flipCannon();
    }

    public Castle getCastle() {
        return castle;
    }

    public void setCastle(Castle castle) {
        this.castle = castle;
    }

    public RegularCannon getCannon() {
        return cannon;
    }

    public void setCannon(RegularCannon cannon) {
        this.cannon = cannon;
    }

    public RegularProjectile getBall() {
        return ball;
    }

    public void setBall(RegularProjectile ball) {
        this.ball = ball;
    }

    public void endTurn(){
        this.ball.getProjectile().reset(this.cannon.getCannon().getX()+this.cannon.getCannon().getWidth()/2,this.cannon.getCannon().getY()+this.cannon.getCannon().getHeight()/2);
        this.cannon.getCannon().setFired(false);
    }


}
