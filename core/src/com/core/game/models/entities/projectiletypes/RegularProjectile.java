package com.core.game.models.entities.projectiletypes;


import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.core.game.models.components.Projectile;
import com.core.game.models.entities.cannons.RegularCannon;

public class RegularProjectile {

    private Projectile projectile;
    private Sprite ballSprite;
    private Texture badBall;
    private SpriteBatch batch;

    public RegularProjectile(){this.projectile = new Projectile();}

    public void Init(float x, float y, int width, int height, double damage) {
        projectile.Init(x, y, width, height, damage);
        this.badBall = new Texture("Cannonball.PNG");
        this.ballSprite = new Sprite(badBall);
        this.batch = new SpriteBatch();
    }

    public Projectile getProjectile() {
        return projectile;
    }

    public SpriteBatch getBatch(){
        return this.batch;
    }

    public void  render(RegularCannon cannon){
            if (this.getProjectile().getY() <= 0) {
                cannon.getCannon().setFired(false);
                this.getProjectile().reset(
                        cannon.getCannon().getX() + cannon.getCannon().getWidth() / 2,
                        cannon.getCannon().getY() + cannon.getCannon().getHeight() / 2);
                //This will return the projectile to the player's cannon. Either
                //this.getProjectile().setX(cannon.getCannon().getX()+cannon.getCannon().getWidth()/2);
                //this.getProjectile().setY(cannon.getCannon().getY()+cannon.getCannon().getHeight()/2);
            }

            if(cannon.getCannon().isFired()) {
                batch.begin();
                batch.draw(ballSprite, this.getProjectile().getX(), this.getProjectile().getY(),
                        this.getProjectile().getWidth(), this.getProjectile().getHeight());
                batch.end();
            }

            this.getProjectile().move(cannon.getCannon().isFired());

    }
    public void dispose(){
        badBall.dispose();
        batch.dispose();

    }

}
