package com.core.game.models.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.core.game.models.components.castlecomponents.BackTowerComponent;
import com.core.game.models.components.castlecomponents.FrontTowerComponent;
import com.core.game.models.components.castlecomponents.KingComponent;
import com.core.game.models.components.castlecomponents.CastleComponent;
import com.core.game.models.components.castlecomponents.RoofComponent;
import com.core.game.models.components.Projectile;
import com.core.game.models.entities.player.Player;

import java.util.ArrayList;

public class Castle {


    private int x, y, width, height;
    private boolean player;
    private ArrayList<CastleComponent> components;

    private Sprite kingSprite, backSprite, frontSprite, roofSprite, backgroundSprite;
    private Texture kingTexture, backTexture, frontTexture, roofTexture, backgorundTexture;
    private SpriteBatch batch;
    private ArrayList<Sprite> spriteList;
    private ArrayList<Texture> textures;
    private Stage stage;
    private Actor background;

    private ArrayList<CastleComponent> deadComponents;



    public Castle(){};

    public void Init(int x, int y, boolean p1){
        this.x = x;
        this.y = y;
        this.width = 450;
        this.height = 270;

        this.components = new ArrayList<>();
        this.spriteList = new ArrayList<>();
        this.textures = new ArrayList<>();
        this.deadComponents = new ArrayList<>();


        //Boolean to check if player is p1 or p2, i.e. player or opponent.
        //Used to flip the King in the opponent castle.
        this.player = p1;


        /* Since the player and opponent castles are mirrored on the battlefield, it is preferable
         * that they are rendered as such. That's why there's and if-else statement here.
         * The back wall of the player has the index 1 in the internal component list, while the
         * index of the back wall of the opponent is 3. The front wall has the index 3 and 1
         * respectively.
         */
        if (player){
            KingComponent king = new KingComponent();
            king.Init(this.x, this.y );
            addComponent(king);
            BackTowerComponent backTower = new BackTowerComponent();
            backTower.Init(this.x, this.y, Player.getInstance().getBthp());
            addComponent(backTower);
            RoofComponent roof = new RoofComponent();
            roof.Init(this.x , this.y , Player.getInstance().getRhp());
            addComponent(roof);
            FrontTowerComponent frontTower = new FrontTowerComponent();
            frontTower.Init(this.x , this.y, Player.getInstance().getFthp());
            addComponent(frontTower);

            this.kingTexture = new Texture("castle/King.png");
            textures.add(kingTexture);
            this.backTexture= new Texture("castle/Castle_back.png");
            textures.add(backTexture);
            this.roofTexture = new Texture("castle/Castle_roof.png");
            textures.add(roofTexture);
            this.frontTexture = new Texture("castle/Castle_front.png");
            textures.add(frontTexture);

            this.kingSprite = new Sprite(kingTexture);

            spriteList.add(kingSprite);
            this.backSprite = new Sprite(backTexture);
            spriteList.add(backSprite);
            this.roofSprite= new Sprite(roofTexture);
            spriteList.add(roofSprite);
            this.frontSprite = new Sprite(frontTexture);
            spriteList.add(frontSprite);
        } else {
            KingComponent king = new KingComponent();
            king.Init(this.x, this.y );
            addComponent(king);
            FrontTowerComponent frontTower = new FrontTowerComponent();
            frontTower.Init(this.x , this.y, 50);
            addComponent(frontTower);
            RoofComponent roof = new RoofComponent();
            roof.Init(this.x , this.y , 30);
            addComponent(roof);
            BackTowerComponent backTower = new BackTowerComponent();
            backTower.Init(this.x, this.y, 30);
            addComponent(backTower);

            this.kingTexture = new Texture("castle/King.png");
            textures.add(kingTexture);
            this.frontTexture = new Texture("castle/Castle_front.png");
            textures.add(frontTexture);
            this.roofTexture = new Texture("castle/Castle_roof.png");
            textures.add(roofTexture);
            this.backTexture= new Texture("castle/Castle_back.png");
            textures.add(backTexture);

            this.kingSprite = new Sprite(kingTexture);
            this.kingSprite.flip(true, false);

            spriteList.add(kingSprite);
            this.frontSprite = new Sprite(frontTexture);
            spriteList.add(frontSprite);
            this.roofSprite= new Sprite(roofTexture);
            spriteList.add(roofSprite);
            this.backSprite = new Sprite(backTexture);
            spriteList.add(backSprite);
        }

        this.backgorundTexture = new Texture("castle/Castle_background.png");
        this.backgroundSprite = new Sprite(backgorundTexture);
        backgroundSprite.setPosition(this.x, this.y);

        this.batch = new SpriteBatch();

    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isPlayer() {
        return player;
    }

    public void setPlayer(boolean player) {
        this.player = player;
    }

    public ArrayList<CastleComponent> getDeadComponents() {
        return deadComponents;
    }

    public void setDeadComponents(ArrayList<CastleComponent> deadComponents) {
        this.deadComponents = deadComponents;
    }

    public ArrayList<CastleComponent> getComponents() {
        return components;
    }

    public void setComponents(ArrayList<CastleComponent> components) {
        this.components = components;
    }

    public void removeComponent(int index){
        this.components.remove(index);
    }

    public void addComponent(CastleComponent component){
        this.components.add(component);
    }

    public Castle getCastle() { return this;}

    public void renderBackground() {
        backgroundSprite.draw(batch);
    }


    public void  render(){
        batch.begin();
        renderBackground();
        for (int i = 0; i<this.components.size(); i++){
            if (this.components.get(i).getHealth() > 0){
                batch.draw(spriteList.get(i), this.x, this.y);
            }
        }
        batch.end();
    }


    public void  renderBuild(){
        int width = Gdx.graphics.getWidth();
        int displacementX;
        if (width <= 2000){
            displacementX = getX();
        }else {
            displacementX = Gdx.graphics.getWidth()/24;
        }
/*
        int displacementY = Gdx.graphics.getHeight()/2-p.getCastle().getHeight();
*/
        int displacementY = Gdx.graphics.getHeight()/2-getHeight();
        batch.begin();
        for (int i = 1; i<this.components.size(); i++){
            batch.draw(spriteList.get(i), (float)(displacementX + 10*i), displacementY+2*this.y);
        }
        batch.end();
    }


    public void dispose(){
        for (Texture  t : textures){
            t.dispose();
        }
        batch.dispose();
    }

    public void update(){
        int index = -1;
        for (CastleComponent c : this.getComponents()){
            if (c.isRemovable()){
                this.deadComponents.add(c);
                index = this.getComponents().indexOf(c);
            }
        }
        while (this.deadComponents.size() > 0){
            this.getComponents().remove(getDeadComponents().get(0));
            this.getCastle().spriteList.remove(index);
            this.deadComponents.remove(getDeadComponents().get(0));
            if(this.deadComponents.size() == 0){
                break;
            }
        }
    }

    public Rectangle getBoundingRect(){
        return new Rectangle(this.getX(), this.getY(), this.getWidth(), this.getHeight());
    }

    public boolean hitBy(Projectile projectile) {
        return getBoundingRect().overlaps(projectile.getBoundingRect());
    }
}
