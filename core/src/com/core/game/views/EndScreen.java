package com.core.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.core.game.MainGame;
import com.core.game.controllers.EventManager;
import com.core.game.models.entities.player.Player;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class EndScreen implements Screen{

    private SpriteBatch batch;
    protected Stage stage;
    private Viewport viewport;
    private OrthographicCamera camera;
    private TextureAtlas atlas;
    protected Skin skin;
    private Label Text;
    private Label space;
    private Texture backTexture;
    private Sprite background;
    private TextButton exitButton;
    
    public EndScreen(){
        atlas = new TextureAtlas("LD buttons.txt");
        skin = new Skin(Gdx.files.internal("data/LD buttons.json"), atlas);

        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FitViewport(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(), camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

        stage = new Stage(viewport, batch); 

        Text = new Label("", skin, "tutorial");
        Text.setAlignment(Align.center);
        Text.setFontScale(2);
        space = new Label("", skin);
    

        backTexture = new Texture(Gdx.files.internal("Menu BG.png"));
        background = new Sprite(backTexture);
        background.setScale(0.8f, 0.8f);

        //Create button
        exitButton = new TextButton("Return", skin, "smallButton");

        exitButton.getLabel().setFontScale(3);

        //Add listener to button
        exitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EventManager.getInstance().changeScreen(0);
                EventManager.getInstance().playMusic(MainGame.music.get(0));
            }
        });
    }

    @Override
    public void show() {
        //Stage should controll input:
        Gdx.input.setInputProcessor(stage);

        //Create Table
        Table mainTable = new Table();
        //Set table to fill stage
        mainTable.setFillParent(true);
        //Set alignment of contents in the table.
        mainTable.top();

        //Change text based on result of battle
        setText();

        //Add actors to table
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(Text);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(exitButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);

        //Add table to stage
        stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.5f, .4f, .25f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        background.setCenter(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/2f);
        background.draw(batch);
        batch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        skin.dispose();
        atlas.dispose();
        backTexture.dispose();
    }

    private void setText(){
        if(Player.getInstance().getCastle().getComponents().get(0).isAlive()){
            Text.setText("\nCongratulations! \n" +
                    "      You have conquered your opponent and proven your     \n " +
                    "might as a ruler of Chelimar.\n " +
                    "  You have gathered 100 gold pieces from   \n"+
                    "your fallen opponent.   \n\n");
            EventManager.getInstance().playSound(MainGame.sounds.get(2));
        }
        else if (!Player.getInstance().getCastle().getComponents().get(0).isAlive()){
            Text.setText("\nDefeat!\n" +
                    "  You have been bested by your opponent and your king is dead.\n"+
                            "        Luckily, there is an heir to the throne, ready to take his place as king.        \n"+
                            "During your battle, you manage to gather 40 gold pieces \n" +
                            "from your opponent\n\n");
            EventManager.getInstance().playSound(MainGame.sounds.get(3));
        }
        else{ //Dette blir for om motstander overgir seg
            Text.setText("\nYour opponent has surrendered! \n"+
                            "       It seems they were no match for your might and have ran off       \n"+
                            "You have gathered 100 gold pieces   \n" +
                            "from your cowardly opponent\n\n");
            EventManager.getInstance().playSound(MainGame.sounds.get(2));
        }
    }
}
