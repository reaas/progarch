package com.core.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.core.game.controllers.EventManager;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class TutorialScreen implements Screen{

    private SpriteBatch batch;
    protected Stage stage;
    private Viewport viewport;
    private OrthographicCamera camera;
    private TextureAtlas atlas;
    protected Skin skin;
    private Label tutorialText;
    private Label space;
    private Texture backTexture;
    private Sprite background;
    private TextButton exitButton;
    
    public TutorialScreen(){
        atlas = new TextureAtlas("LD buttons.txt");
        skin = new Skin(Gdx.files.internal("data/LD buttons.json"), atlas);

        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FitViewport(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(), camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

        stage = new Stage(viewport, batch); 

        tutorialText = new Label("\n \n About upgrades: \n"+
                                "In the building screen you can use gold gained in battle to upgrade parts of your castle. \n"+
                                "As you upgrade your castle the cost of new upgrades increases along with the strength of the structures. \n"+
                                "About battles: \n"+
                                "In a battle, you and your opponent take turns firing upon each other. \n"+
                                "To fire, touch the screen and drag back. The length of the line you draw will determine the power of the shot \n"+
                                "and the angle of the line in relation to the ground sets the angle of the shot. The angle must be between 0 and 90 degrees \n"+
                                "                                   "+
                                "in order to fire, meaning the shot needs to head towards the right. When the angle and power is set you can either draw a new"+
                                "                                   \n"+
                                "line to set a new power and angle, or press the fire button in the top right to fire using the parameters set by your latest input. \n"+
                                "If you at any point want to leave the battle, hit the surrender button in the top left. If you hit your opponents king, you win!\n \n \n \n ", skin, "tutorial");
        tutorialText.setAlignment(Align.center);
        tutorialText.setFontScale(2);
        space = new Label("", skin);
    

        backTexture = new Texture(Gdx.files.internal("Menu BG.png"));
        background = new Sprite(backTexture);
        background.setScale(0.8f, 0.8f);

        //Create button
        exitButton = new TextButton("Return", skin, "smallButton");

        exitButton.getLabel().setFontScale(3);

        //Add listener to button
        exitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EventManager.getInstance().changeScreen(0);
            }
        });
    }

    @Override
    public void show() {
        //Stage should controll input:
        Gdx.input.setInputProcessor(stage);

        //Create Table
        Table mainTable = new Table();
        //Set table to fill stage
        mainTable.setFillParent(true);
        //Set alignment of contents in the table.
        mainTable.top();

        //Add actors to table
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(tutorialText);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(exitButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);

        //Add table to stage
        stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.5f, .4f, .25f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        background.setCenter(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/2f);
        background.draw(batch);
        batch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        skin.dispose();
        atlas.dispose();
        backTexture.dispose();
    }
}
