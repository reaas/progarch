package com.core.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.core.game.controllers.EventManager;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.core.game.models.entities.player.Player;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class RegisterScreen implements Screen {

    private SpriteBatch batch;
    protected Stage stage;
    private Viewport viewport;
    private OrthographicCamera camera;
    private TextureAtlas atlas;
    protected Skin skin;
    private Label space;
    private Texture backTexture;
    private Sprite background;
    private TextField email;
    private TextField password;
    private TextField password2;
    private TextButton registerButton;
    private TextButton returnButton;
    private TextButton exitButton;

    public RegisterScreen(){
        atlas = new TextureAtlas("LD buttons.txt");
        skin = new Skin(Gdx.files.internal("data/LD buttons.json"), atlas);

        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FitViewport(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(), camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

        stage = new Stage(viewport, batch);

        space = new Label("", skin);

        backTexture = new Texture(Gdx.files.internal("Menu BG.png"));
        background = new Sprite(backTexture);
        background.setScale(0.8f, 0.8f);

        //Create textfields
        email = new TextField("", skin);
        password = new TextField("", skin);
        password2 = new TextField("", skin);
        email.setMessageText("E-mail");
        password.setMessageText("Password");
        password2.setMessageText("Re-enter Password");

        //Create buttons
        registerButton = new TextButton("Register", skin, "smallButton");
        returnButton = new TextButton("Return", skin, "smallButton");
        exitButton = new TextButton("Exit", skin, "smallButton");

        registerButton.getLabel().setFontScale(3);
        returnButton.getLabel().setFontScale(3);
        exitButton.getLabel().setFontScale(3);

        //Add listeners to buttons
        registerButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(password.getText().equals(password2.getText())) {
                    if(password2.getText().length() <= 5) {
                        password2.setMessageText("Password too short!");
                    } else {
                        EventManager.getInstance().register(email.getText(), password.getText());
                        Gdx.input.setOnscreenKeyboardVisible(false); // Hides keyboard
                        EventManager.getInstance().changeScreen(0); //go to main menu
                    }
                } else {
                    password2.setMessageText("Password does not match!");
                }
            }
        });
        returnButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.input.setOnscreenKeyboardVisible(false); // Hides keyboard
                if(EventManager.getInstance().isSignedIn()) {
                    EventManager.getInstance().changeScreen(0); // MainMenu screen
                } else {
                    EventManager.getInstance().changeScreen(6); // Log in screen
                }
            }
        });
        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });
    }

    @Override
    public void show() {
        //Stage should control input:
        Gdx.input.setInputProcessor(stage);

        //Create Table
        Table mainTable = new Table();
        //Set table to fill stage
        mainTable.setFillParent(true);
        //Set alignment of contents in the table.
        mainTable.top();

        //Add actors to table
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(email).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);;
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(password).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);;
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(password2).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);;
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(registerButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);;
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(returnButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);;
        mainTable.row();
        mainTable.add(exitButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);;


        //Add table to stage
        stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.5f, .4f, .25f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        background.setCenter(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/2f);
        background.draw(batch);
        batch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        skin.dispose();
        atlas.dispose();
    }
}
