package com.core.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.core.game.controllers.EventManager;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.core.game.models.entities.player.Player;

public class MainMenuScreen implements Screen {

    private SpriteBatch batch;
    protected Stage stage;
    private Viewport viewport;
    private OrthographicCamera camera;
    private TextureAtlas atlas;
    protected Skin skin;
    private Label space;
    private Texture logoTexture;
    private Sprite logo;
    private Texture backTexture;
    private Sprite background;
    private TextButton playButton;
    private TextButton buildButton;
    private TextButton tutorialButton;
    private TextButton optionsButton;
    private TextButton logOutButton;
    private TextButton exitButton;

    public MainMenuScreen() {
        atlas = new TextureAtlas("LD buttons.txt");
        skin = new Skin(Gdx.files.internal("data/LD buttons.json"), atlas);
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

        stage = new Stage(viewport, batch);

        space = new Label("", skin);
        logoTexture = new Texture("Long logo blue.png");
        logo = new Sprite(logoTexture);
        logo.setScale(0.5f, 0.5f);

        backTexture = new Texture(Gdx.files.internal("Menu BG.png"));
        background = new Sprite(backTexture);
        background.setScale(0.8f, 0.8f);


        // Create buttons
        playButton = new TextButton("Play", skin, "smallButton");
        buildButton = new TextButton("Build", skin, "smallButton");
        tutorialButton = new TextButton("Tutorial", skin, "smallButton");
        optionsButton = new TextButton("Options", skin, "smallButton");
        logOutButton = new TextButton("Log out", skin, "smallButton");
        exitButton = new TextButton("Exit", skin, "smallButton");

        playButton.getLabel().setFontScale(3);
        buildButton.getLabel().setFontScale(3);
        tutorialButton.getLabel().setFontScale(3);
        optionsButton.getLabel().setFontScale(3);
        logOutButton.getLabel().setFontScale(3);
        exitButton.getLabel().setFontScale(3);

        // Add listeners to buttons
        playButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EventManager.getInstance().changeScreen(2); //3 for straight to battle screen, 2 is loading screen(value in finished game)
                EventManager.getInstance().findMatch(); // un
            }
        });
        buildButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EventManager.getInstance().changeScreen(4);
            }
        });
        tutorialButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EventManager.getInstance().changeScreen(5);
            }
        });
        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EventManager.getInstance().changeScreen(1);
            }
        });
        logOutButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EventManager.getInstance().logout();
                EventManager.getInstance().changeScreen(6);
            }
        });
        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.exit();
            }
        });
    }

    @Override
    public void show() {
        //Stage should controll input:
        Gdx.input.setInputProcessor(stage);

        //Create Table
        Table mainTable = new Table();
        //Set table to fill stage
        mainTable.setFillParent(true);
        //Set alignment of contents in the table.
        mainTable.top();

        //Add actors to table
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(space);
        mainTable.row();
        mainTable.add(playButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);
        mainTable.row();
        mainTable.add(buildButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);
        mainTable.row();
        mainTable.add(tutorialButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);
        mainTable.row();
        mainTable.add(optionsButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);
        mainTable.row();
        mainTable.add(logOutButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);
        mainTable.row();
        mainTable.add(exitButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/8);


        // Resetting the castle health back to normal when Main Menu is shown
        Player.getInstance().getCastle().getComponents().get(0).setHealth(1);
        Player.getInstance().getCastle().getComponents().get(1).setHealth(Player.getInstance().getBthp());
        Player.getInstance().getCastle().getComponents().get(2).setHealth(Player.getInstance().getRhp());
        Player.getInstance().getCastle().getComponents().get(3).setHealth(Player.getInstance().getFthp());

        //Add table to stage
        stage.addActor(mainTable);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.5f, .4f, .25f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        
        batch.begin();
        background.setCenter(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/2f);
        logo.setCenter(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/1.1f);
        background.draw(batch);
        logo.draw(batch);
        batch.end();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        skin.dispose();
        atlas.dispose();
        backTexture.dispose();
    }
}
