package com.core.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.core.game.controllers.EventManager;
import com.core.game.models.entities.player.Player;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.HashMap;
import java.util.Map;

import static com.core.game.models.entities.player.Player.MAX_HEALTH;

public class BuildScreen implements Screen{

    private SpriteBatch batch;
    protected Stage stage;
    private Viewport viewport;
    private OrthographicCamera camera;
    private TextureAtlas atlas;
    protected Skin skin;
    private Label balance;
    private Label price1;
    private Label price2;
    private Label price3;
    private Label hp1;
    private Label hp2;
    private Label hp3;
    private Label space;
    private Texture backTexture;
    private Sprite background;
    private Table mainTable;
    private Container<Table> tableContainer;
    private Table moneyTable;
    private Container<Table> tableContainer2;
    private TextButton upButton1;
    private TextButton upButton2;
    private TextButton upButton3;
    private TextButton exitButton;
    private Player p;
    
    public BuildScreen(){
        atlas = new TextureAtlas("LD buttons.txt");
        skin = new Skin(Gdx.files.internal("data/LD buttons.json"), atlas);
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FitViewport(Gdx.graphics.getWidth(),Gdx.graphics.getHeight(), camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

        this.p = Player.getInstance();
        //Castle c = p.getCastle();
        stage = new Stage(viewport, batch);

        balance = new Label("", skin, "coins"); 
        balance.setAlignment(Align.left);
        balance.setFontScale(3);
        price1 = new Label("", skin, "labelMed"); 
        price1.setAlignment(Align.center);
        price1.setFontScale(3);
        price2 = new Label("", skin, "labelMed"); 
        price2.setAlignment(Align.center);
        price2.setFontScale(3);
        price3 = new Label("", skin, "labelMed"); 
        price3.setAlignment(Align.center);
        price3.setFontScale(3);

        int displacementX = Gdx.graphics.getWidth()/8;
        int displacementY = Gdx.graphics.getHeight()/2-p.getCastle().getHeight();
        hp1 = new Label("HP:\t100", skin);
        hp1.setPosition(displacementX - 7*p.getCastle().getWidth()/16, displacementY + 2*p.getCastle().getHeight()/3);
        hp2 = new Label("HP:\t100", skin);
        hp2.setPosition(displacementX-p.getCastle().getWidth()/16 , displacementY + p.getCastle().getHeight());
        hp3 = new Label("HP:\t100", skin);
        hp3.setPosition(displacementX + 3*p.getCastle().getWidth()/8, displacementY + 2*p.getCastle().getHeight()/3);

        hp1.setFontScale(2);
        hp2.setFontScale(2);
        hp3.setFontScale(2);

        space = new Label("",skin);

        backTexture = new Texture(Gdx.files.internal("Menu BG.png"));
        background = new Sprite(backTexture);
        background.setScale(0.8f, 0.8f);

        upButton1 = new TextButton("Upgrade back tower", skin, "mediumButton");
        upButton2 = new TextButton("Upgrade roof", skin, "mediumButton");
        upButton3 = new TextButton("Upgrade front tower", skin, "mediumButton");
        exitButton = new TextButton("Return", skin, "mediumButton");

        upButton1.getLabel().setFontScale(3);
        upButton2.getLabel().setFontScale(3);
        upButton3.getLabel().setFontScale(3);
        exitButton.getLabel().setFontScale(3);

        //Add listeners to buttons
       upButton1.addListener(new ClickListener(){
        @Override
        public void clicked(InputEvent event, float x, float y) {
            EventManager.getInstance().buy(1, p.getPrices().get(0));
        }
        });
        upButton2.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
            EventManager.getInstance().buy(2, p.getPrices().get(1));
            }
        });
        upButton3.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
            EventManager.getInstance().buy(3, p.getPrices().get(2));
            }
        });
        exitButton.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Map<String, String> userData = new HashMap<>();
                userData.put("Balance", Integer.toString(Player.getInstance().getBalance()));
                userData.put("bthp", Integer.toString(Player.getInstance().getBthp()));
                userData.put("fthp", Integer.toString(Player.getInstance().getFthp()));
                userData.put("rhp", Integer.toString(Player.getInstance().getRhp()));
                EventManager.getInstance().updateUserData(userData);
                EventManager.getInstance().changeScreen(0);
            }
        });
    }

    @Override
    public void show() {
       tableContainer = new Container<Table>();
       mainTable = new Table();
       tableContainer2 = new Container<Table>();
       moneyTable = new Table();
 
       //Stage should controll input:
       Gdx.input.setInputProcessor(stage);      
       //Set table to fill stage
       mainTable.setFillParent(true);
       //Set alignment of contents in the table.
       mainTable.top();
       moneyTable.setFillParent(true);
       //Set alignment of contents in the table.
       moneyTable.top();
  
       float sw = Gdx.graphics.getWidth();
       float sh = Gdx.graphics.getHeight();

       float cw = sw * 0.7f;
       float ch = sh * 0.5f;
       tableContainer.setSize(cw, ch);
       tableContainer.setPosition((sw - cw) / 2.0f, (sh - ch) / 1.0f);
       tableContainer.fillX();
       tableContainer2.setSize(cw, ch/1.2f);
       tableContainer2.setPosition((sw - cw) / 2.0f, (sh - ch) / 1.0f);
       tableContainer2.fillX();

       moneyTable.add(space);
       moneyTable.row();
       moneyTable.add(space);
       moneyTable.row();
       moneyTable.add(space);
       moneyTable.row();
       moneyTable.add(balance);
       mainTable.add(space);
       mainTable.row();
       mainTable.add(space);
       mainTable.row();
       mainTable.add(space);
       mainTable.row();
       mainTable.add(space);
       mainTable.row();
       mainTable.add(upButton1).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/7);
       mainTable.add(price1).width(cw/4.0f);
       mainTable.row();
       mainTable.add(upButton2).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/7);
       mainTable.add(price2).width(cw/4.0f);
       mainTable.row();
       mainTable.add(upButton3).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/7);
       mainTable.add(price3).width(cw/4.0f);
       mainTable.row();
       mainTable.add(exitButton).width(Gdx.graphics.getWidth()/4).height(Gdx.graphics.getHeight()/7);

       //Add table to stage
       tableContainer2.setActor(moneyTable);
       stage.addActor(tableContainer2);
       tableContainer.setActor(mainTable);
       stage.addActor(tableContainer);
       stage.addActor(hp1);
       stage.addActor(hp2);
       stage.addActor(hp3);

        for (int i = 0; i<3; ++i){
            p.setPrice(i, p.getCastle().getComponents().get(i+1).getHealth());
        }
        updatePrices();
    }

    public void updateHP(){
        hp1.setText("HP:\t "+p.getCastle().getComponents().get(1).getHealth());
        hp2.setText("HP:\t "+p.getCastle().getComponents().get(2).getHealth());
        hp3.setText("HP:\t "+p.getCastle().getComponents().get(3).getHealth());

    }

    public void updatePrices(){

        if(p.getBthp() >= MAX_HEALTH){
            price1.setText("\n All upgrades bought \n");
        }
        else{
            price1.setText("\n Price: "+String.valueOf(p.getPrices().get(0))+"\n");
        }
        if(p.getRhp() >= MAX_HEALTH){
            price2.setText("\n All upgrades bought \n");
        }
        else{
            price2.setText("\n Price: "+String.valueOf(p.getPrices().get(1))+"\n");
        }
        if(p.getFthp() >= MAX_HEALTH){
            price3.setText("\n All upgrades bought \n");
        }
        else{
            price3.setText("\n Price: "+String.valueOf(p.getPrices().get(2))+"\n");
        }
    }


    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(.5f, .4f, .25f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        background.setCenter(Gdx.graphics.getWidth()/2f, Gdx.graphics.getHeight()/2f);
        background.draw(batch);
        p.getCastle().renderBuild();
        batch.end();


        balance.setText("\n     "+p.getBalance()+"                 \n");
        updatePrices();
        updateHP();

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        skin.dispose();
        atlas.dispose();
        backTexture.dispose();
        batch.dispose();

    }
}
