package com.core.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.core.game.models.entities.cannons.RegularCannon;
import com.core.game.models.components.Explosion;
import com.core.game.models.components.castlecomponents.CastleComponent;
import com.core.game.controllers.EventManager;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.core.game.models.Timer;
import com.core.game.models.entities.player.Opponent;
import com.core.game.models.entities.player.Player;
import java.util.ArrayList;

public class BattleScreen implements Screen {

    private SpriteBatch batch;
    protected Stage stage;
    private Viewport viewport;
    private OrthographicCamera camera;
    private TextureAtlas atlas;
    protected Skin skin;
    private TextButton fireButton;
    private TextButton exitButton;
    private Container<Table> tableContainer;
    private Table mainTable;
    private Label space;


    private ArrayList<Explosion> explosions;
    private ArrayList<Explosion> explToRemove;

    private Vector2 touchPos = new Vector2();
    private Vector2 dragPos = new Vector2();
    private float distance = 0;
    private float angle = 0;

    private RegularCannon canon = new RegularCannon();

    public static Texture backgroundTexture;
    private SpriteBatch spriteBatch;
    private Player player;
    private Opponent opponent;

    private Texture powerBar;
    private Texture power;
    private Texture fuse;
    private Texture bomb;

    private Sprite powerBarSprite;
    private Sprite powerSprite;
    private TextureRegion fuseSprite;


    private Timer timer;
    private BitmapFont font;




    public BattleScreen() {
        atlas = new TextureAtlas("LD buttons.txt");
        skin = new Skin(Gdx.files.internal("data/LD buttons.json"), atlas);
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();


        timer = Timer.getInstance();
        timer.Intit(15f);
        font =new BitmapFont();
        font.getData().scale(3);
        
        explosions = new ArrayList<>();
        explToRemove = new ArrayList<>();


        player = Player.getInstance();
        player.Init();
        opponent = Opponent.getInstance();
        opponent.Init();
        //Default value of the angle for canon of the opponent.
        opponent.getCannon().getCannon().setAngle(135f);


        backgroundTexture = new Texture("Background.PNG");

        spriteBatch = new SpriteBatch();

        powerBar = new Texture("powerBar.PNG");
        powerBarSprite = new Sprite(powerBar);
        power = new Texture("power.PNG");
        powerSprite = new Sprite(power);
        fuse = new Texture("fuse.PNG");
        fuseSprite = new TextureRegion(fuse);
        bomb = new Texture("bomb.PNG");

        stage = new Stage(viewport, batch);
        stage.addListener(new ClickListener(){
            @Override
            public boolean touchDown(InputEvent event, float screenX, float screenY, int pointer, int button){
                if((screenY>50 && screenY<=Gdx.graphics.getHeight()-50) && !Player.getInstance().waiting){
                    touchPos.set(screenX, Gdx.graphics.getHeight() - screenY);
                }
                return true;
            }
            @Override
            public void touchDragged(InputEvent event, float screenX, float screenY, int pointer){
                if((screenY>50 && screenY<=Gdx.graphics.getHeight()-50) && !Player.getInstance().waiting){
                    dragPos.set(screenX, Gdx.graphics.getHeight() - screenY);
                    distance = touchPos.dst(dragPos);
                    angle = MathUtils.atan2(-touchPos.x + dragPos.x, dragPos.y - touchPos.y) * MathUtils.radiansToDegrees+90;
                }  
            }
            @Override
            public void touchUp(InputEvent event, float screenX, float screenY, int pointer, int button){
                if((angle>0 && distance>0) && !Player.getInstance().waiting){
                    EventManager.getInstance().setParameters(distance, angle);
                }
            }
        });

        space = new Label("", skin);

        // Create buttons
        fireButton = new TextButton("Fire", skin, "smallButton");
        exitButton = new TextButton("Surrender", skin, "smallButton");

        fireButton.getLabel().setFontScale(3);
        exitButton.getLabel().setFontScale(3);
    }

    @Override
    public void show() {
        // Add listeners to buttons
        fireButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
            if(!player.getCannon().getCannon().isFired() && !Player.getInstance().waiting){
                EventManager.getInstance().fire(player.getCannon().getCannon(), player.getBall().getProjectile());
                timer.reset();
            }
            }
        });
        exitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                EventManager.getInstance().surrender();
                EventManager.getInstance().changeScreen(0);
            }
        });

        tableContainer = new Container<Table>();
        mainTable = new Table();

        //Stage should controll input:
        Gdx.input.setInputProcessor(stage);
        //Set table to fill stage
        mainTable.setFillParent(true);
        //Set alignment of contents in the table.
        mainTable.top();

        float sw = Gdx.graphics.getWidth();
        float sh = Gdx.graphics.getHeight();

        float cw = sw * 0.7f;
        float ch = sh * 0.5f;
        tableContainer.setSize(cw, ch);
        tableContainer.setPosition((sw - cw) / 2.0f, (sh - ch) / 2.0f);
        tableContainer.fillX();

        //Add actors to table
        mainTable.add(exitButton).width(cw/5.0f).height(ch/4);
        mainTable.add(space).width(cw/1.0f);
        mainTable.add(fireButton).width(cw/5.0f).height(ch/4);;
        
        //Add table to stage
        stage.addActor(mainTable);
    }


    public void updateExpl(){
        for (Explosion e : explosions){
            e.update(Gdx.graphics.getDeltaTime());
            if (e.remove) {
                explToRemove.add(e);
            }
        }
        explosions.removeAll(explToRemove);
    }

    public void renderExpl(){
        for (Explosion e : explosions){
            e.render(spriteBatch);
        }
    }

    public void hitDetect(Player p){
        // Hit detection:
        if (p.getCannon().getCannon().isFired() && p.getBall().getProjectile().isMoving()) {
            if (opponent.getCastle().hitBy(p.getBall().getProjectile())) {
                for (CastleComponent c : opponent.getCastle().getComponents()) {
                    if (c.getHealth() > 0) {
                        if (c.hitBy(player.getBall().getProjectile())) {
                            c.getDamagedBy(player.getBall().getProjectile());
                            explosions.add(new Explosion(p.getBall().getProjectile().getX(), p.getBall().getProjectile().getY()));
                            EventManager.getInstance().hit();
                            player.endTurn();

                        }
                    }
                }
            }
        }
    }

    /*
     * This hit detection works just like when the player shoots. However, as the last opponents
     * shot is only rendered for show and not for actual damage, the getDamagedBy() method is
     * commented out
     */
    public void hitDetect(Opponent p){
        // Hit detection:
        if (p.getCannon().getCannon().isFired() && p.getBall().getProjectile().isMoving()) {
            if (player.getCastle().hitBy(p.getBall().getProjectile())) {
                for (CastleComponent c : player.getCastle().getComponents()) {
                    if (c.getHealth() > 0) {
                        if (c.hitBy(p.getBall().getProjectile())) {
                            //c.getDamagedBy(p.getBall().getProjectile());
                            explosions.add(new Explosion(p.getBall().getProjectile().getX(), p.getBall().getProjectile().getY()));
                            EventManager.getInstance().hit();
                            p.endTurn();


                        }
                    }
                }
            }
        }
    }

    @Override
    public void render(float delta) {

        if(!player.getCannon().getCannon().isFired() && timer.isTimeOut()){
            timer.reset();
            //If the timer goes to zero, it automatically fires the cannon.
            if(!player.waiting){
                EventManager.getInstance().fire(player.getCannon().getCannon(), player.getBall().getProjectile());
            }
        }

        timer.updateTime(Gdx.graphics.getDeltaTime());

        Gdx.gl.glClearColor(.5f, .4f, .25f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        fuseSprite.setRegionWidth((int)(fuse.getWidth()*(timer.getTime()/timer.getStartTime())));

        spriteBatch.begin();
        spriteBatch.draw(backgroundTexture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        spriteBatch.draw(bomb,Gdx.graphics.getWidth()/2-fuse.getWidth()/2-bomb.getWidth()/2, Gdx.graphics.getHeight()/1.3f+bomb.getHeight()/4, bomb.getWidth()/2, bomb.getHeight()/2);
        spriteBatch.draw(fuseSprite,Gdx.graphics.getWidth()/2-fuse.getWidth()/2, Gdx.graphics.getHeight()/1.3f);
        spriteBatch.draw(powerSprite,player.getCastle().getX(),Gdx.graphics.getHeight()/2,player.getCastle().getWidth()*(player.getCannon().getCannon().getPower()-20)/30,player.getCastle().getHeight()/3);
        spriteBatch.draw(powerBarSprite,player.getCastle().getX(),Gdx.graphics.getHeight()/2,player.getCastle().getWidth(),player.getCastle().getHeight()/3);
        spriteBatch.end();

        stage.act();
        stage.draw();

        EventManager.getInstance().aim(player.getCannon().getCannon());

        player.getCannon().getBatch().setProjectionMatrix(stage.getCamera().combined);
        player.getBall().getBatch().setProjectionMatrix(stage.getCamera().combined);

        hitDetect(player);
        hitDetect(opponent);

        isWin();

        opponent.getCastle().render();
        opponent.getCannon().render();
        opponent.getBall().render(opponent.getCannon());

        player.getCastle().render();
        player.getCannon().render();
        player.getBall().render(player.getCannon());
        updateExpl();
        renderExpl();

    }

    public void isWin() {
        if (!Opponent.getInstance().getCastle().getComponents().get(0).isAlive()) {
            Player.getInstance().setBalance(Player.getInstance().getBalance() + 100);
            Player.getInstance().save();
            EventManager.getInstance().changeScreen(8);
        } else if (!Player.getInstance().getCastle().getComponents().get(0).isAlive()) {
            Player.getInstance().setBalance(Player.getInstance().getBalance() + 40);
            Player.getInstance().save();
            EventManager.getInstance().changeScreen(8);
        }
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.update();
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose() {
        skin.dispose();
        atlas.dispose();
        canon.dispose();
        player.getBall().dispose();
        player.getCannon().dispose();
        player.getCastle().dispose();
        opponent.getBall().dispose();
        opponent.getCannon().dispose();
        opponent.getCastle().dispose();
        backgroundTexture.dispose();
        powerBar.dispose();
        power.dispose();
        fuse.dispose();
        bomb.dispose();
    }
}