package com.core.game.controllers;

import java.lang.management.PlatformLoggingMXBean;
import java.util.Map;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.core.game.MainGame;
import com.core.game.models.Timer;
import com.core.game.models.components.Cannon;
import com.core.game.models.components.Projectile;
import com.core.game.models.entities.player.Opponent;
import com.core.game.models.entities.player.Player;
import com.core.objects.Match;

import static com.core.game.models.entities.player.Player.MAX_HEALTH;

public class EventManager {
    private float distance = 0; // Sets power of shot
    private float angle = 0; // Sets angle for shot

    private Match currentMatch;

    //Singleton pattern used
    private static final EventManager INSTANCE = new EventManager();

    public EventManager() {
    }

    public static EventManager getInstance() {
        return INSTANCE;
    }

    //Function for changing the active screen
    public void changeScreen(int i) {
        ((Game) Gdx.app.getApplicationListener()).setScreen(MainGame.screens.get(i));
        /*
         * 0 = Main menu, 1 = Settings, 2 = Loading, 3 = Battle, 4 = Build, 
		 * 5 = Tutorial, 6 = LogIn, 7 = Register, 8 = End
         */
    }

    //Function to register purchase from build screen
    public void buy(int i, int price) {
        Player p = Player.getInstance();
        // Back tower
        if (i == 1 && p.getBalance() >= price && p.getBthp()< MAX_HEALTH) {
            p.setBthp(p.getBthp()+20);
            p.setPrice(0, p.getBthp());
            p.setBalance(p.getBalance() - price);
            p.save();
        }
        // Middle
        if (i == 2 && p.getBalance() >= price && p.getRhp()< MAX_HEALTH) {
            p.setRhp(p.getRhp() + 20);
            p.setPrice(1, p.getRhp());
            p.setBalance(p.getBalance() - price);
            p.save();
        }
        // Front tower
        if (i == 3 && p.getBalance() >= price && p.getFthp()< MAX_HEALTH) {
            p.setFthp(p.getFthp() + 20);
            p.setPrice(2, p.getFthp());
            p.setBalance(p.getBalance() - price);
            p.save();
        }

	}

	public void register(String email, String password) {
        MainGame.Firebase.onRegisterButtonClicked(email, password);
    }

    public void login(String email, String password) {
        MainGame.Firebase.onLogInButtonClicked(email, password);
    }

    public void logout() {
        MainGame.Firebase.onLogOutButtonClicked();
    }

    public Boolean isSignedIn() {
        return MainGame.Firebase.isSignedIn();
    }

    public void findMatch() {
        MainGame.Firebase.onPlayButtonClicked();
    }

    public void startMatch(boolean isOpener, String gameRef) {
        Player.getInstance().waiting = !isOpener;
        Player.getInstance().isOpener = isOpener;
        Player.getInstance().gameRef = gameRef;

        Opponent.getInstance().waiting = isOpener;

        currentMatch = new Match(Player.getInstance(), Player.getInstance().isOpener);
        MainGame.Firebase.onUpdateMatch(currentMatch);

        EventManager.getInstance().changeScreen(3);
    }

    /*
     * Used only to show the last shot of the opponent. This is only supposed to render the
     * shot, since the damage is already registered.
     */
    public void opponentFire(Cannon cannon, Projectile ball) {
        Timer.getInstance().reset();

        if (cannon.getPower() == 0 || cannon.getAngle() == 0) {
            return;
        }

        opponentAim(cannon);
        cannon.shoot(ball);
        playSound(MainGame.sounds.get(0)); // Play cannon sound-effect
    }

    /*
     * When the fire button is pushed, the data is to be sent to Firebase is built, as well as
     * the local rendering of the shot is done. Damage calculations are done locally before being
     * sent to the cloud.
     */
	public void fire(Cannon cannon, Projectile ball) {
        if(distance == 0 || angle == 0){
            return;
        }

        // Used on the other device to render the shot
        currentMatch.projectile.power = (int)cannon.getPower();
        currentMatch.projectile.angle = cannon.getAngle();

        cannon.shoot(ball);
        playSound(MainGame.sounds.get(0)); // Play cannon sound-effect
        setParameters(1,1);
    }

    /*
     * Called when the timer runs out
     */
    public void timeout() {
        currentMatch.projectile.power = 0;
        currentMatch.projectile.angle = 0;

        updateMatch();
    }

    public void surrender() {
        if(Player.getInstance().isOpener) {
            currentMatch.player1.isAlive = false;
        } else {
            currentMatch.player2.isAlive = false;
        }

        currentMatch.projectile.angle = 0;
        currentMatch.projectile.power = 0;

        MainGame.Firebase.onUpdateMatch(currentMatch);
    }

    /*
     * Called after the last shot is done rendering (either a hit or miss). As we want to ensure
     * the player that pressed "play" first always is player1 in the cloud, we need to structure the
     * Match-object accordingly
     */
    public void updateMatch() {
        if(Player.getInstance().isOpener) {
            currentMatch.updateMatch(Player.getInstance(), Opponent.getInstance());
            currentMatch.player1.playing = false;
            currentMatch.player1.isAlive = Player.getInstance().getCastle().getComponents().get(0).isAlive();
            currentMatch.player2.playing = true;
            currentMatch.player2.isAlive = Opponent.getInstance().getCastle().getComponents().get(0).isAlive();
        } else {
            currentMatch.updateMatch(Opponent.getInstance(), Player.getInstance());
            currentMatch.player1.playing = true;
            currentMatch.player1.isAlive = Opponent.getInstance().getCastle().getComponents().get(0).isAlive();
            currentMatch.player2.playing = false;
            currentMatch.player2.isAlive = Player.getInstance().getCastle().getComponents().get(0).isAlive();
        }
        MainGame.Firebase.onUpdateMatch(currentMatch);
    }

    public boolean myTurn(){
        if (Player.getInstance().isOpener && currentMatch.player1.playing){
            return true;
        }else {
            return false;
        }
    }

    public void hit(){
        playSound(MainGame.sounds.get(1));
    }

    /*
     * Updates the Firebase database when purchasing new upgrades.
     */
    public void updateUserData(Map userData) {
        MainGame.Firebase.onUpdateUserData(userData);
    }

    public void getUserData() {
        MainGame.Firebase.onGetUserData();
    }


    // Function that sets parameters based on input from battle screen
	public void setParameters(float distance, float angle) {
        this.distance = distance;
        this.angle = angle;
    }
    
    // Toggles sound
    public void toggleSound() {
        Player.soundOn = !Player.soundOn;
        //If sound is turned off, stop the menu music which will be playing
        if(!Player.soundOn){
            MainGame.music.get(0).stop();
        }
        //Otherwise, start the menu music as the sound will be turned on now
        else{
            playMusic(MainGame.music.get(0));
        }
    }

    //Plays a sound-effect
	public void playSound(Sound sound) {
        //If sound off, return
        if(!Player.soundOn){
            return;
        }
        //Otherwise, play the sound
        else{
            sound.play();
        }
	}

    //Plays music
	public void playMusic(Music music) {
        //If sound off, return
        if(!Player.soundOn){
            return;
        }
        //Turn off other music playing
        for(Music m : MainGame.music){
            m.stop();
        }
        //Set music to loop and play
        music.setLooping(true);
        music.play();
	}

	/*
	 * Sets the players cannon position as well as calculating the power used
	 */
	public void aim(Cannon cannon){
        cannon.setAngle(this.angle);
        if (this.angle>90){
            cannon.setAngle(90);
        }
        if (this.angle<10){
            cannon.setAngle(10);
        }
        cannon.setPower(20+(int)this.distance/20);
        if(20+this.distance/20>50){
            cannon.setPower(50);
        }
    }

    /*
     * Only used to set the opponents canon position
     */
    public void opponentAim(Cannon cannon){
        cannon.setAngle(180f-cannon.getAngle());
        if (cannon.getAngle() < 90){
            cannon.setAngle(90);
        }
        if (cannon.getAngle()>170){
            cannon.setAngle(170);
        }
    }
}
