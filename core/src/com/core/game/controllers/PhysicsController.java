package com.core.game.controllers;

import com.core.game.models.components.Projectile;

public class PhysicsController {

    //Here, the singleton pattern is used
    private static final PhysicsController INSTANCE = new PhysicsController();

    private PhysicsController() {}

    public static PhysicsController getInstance() {
        return INSTANCE;
    }


    private float gravity = 1;

    public float getGravity() {
        return gravity;
    }

    public void setGravity(float gravity) {
        this.gravity = gravity;
    }

    public void calculateNextPosition(Projectile projectile){
        projectile.setX(projectile.getX()+projectile.getSpeedX());
        projectile.setSpeedY(projectile.getSpeedY()-this.gravity);
        projectile.setY(projectile.getY()+projectile.getSpeedY());
    }

    public void decomposeSpeed(float power, float angle, Projectile projectile){
        projectile.setSpeedX((float)Math.cos(angle)*power);
        projectile.setSpeedY((float)Math.sin(angle)*power);
    }

}
