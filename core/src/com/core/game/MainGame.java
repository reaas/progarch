package com.core.game;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.core.apis.FirebaseServices;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.core.game.controllers.EventManager;
import com.core.game.models.entities.player.Player;
import com.core.game.views.BattleScreen;
import com.core.game.views.BuildScreen;
import com.core.game.views.EndScreen;
import com.core.game.views.LoadingScreen;
import com.core.game.views.LogInScreen;
import com.core.game.views.MainMenuScreen;
import com.core.game.views.RegisterScreen;
import com.core.game.views.SettingsScreen;
import com.core.game.views.TutorialScreen;

public class MainGame extends Game {
	public static List<Screen> screens = new ArrayList<>();
	public static FirebaseServices Firebase;

	public MainGame(FirebaseServices Firebase) {
		this.Firebase = Firebase;
	}

	//Lists of music and sound-effects. Separate due to Music-class being less memory-intensive
	public static List<Music> music = new ArrayList<>();
	public static List<Sound> sounds = new ArrayList<>();
/*	public Player p;
	public Opponent o;*/

	@Override
	public void create() {
		/*try {*/
			Player p = new Player();
			/*this.p = p;
			p.Init();
			this.o = new Opponent();
			o.Init();*/
		/*} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		MainGame.screens.add(new MainMenuScreen());
		MainGame.screens.add(new SettingsScreen());
		MainGame.screens.add(new LoadingScreen());
		MainGame.screens.add(new BattleScreen());
		MainGame.screens.add(new BuildScreen());
		MainGame.screens.add(new TutorialScreen());
		MainGame.screens.add(new LogInScreen());
		MainGame.screens.add(new RegisterScreen());
		MainGame.screens.add(new EndScreen());
        /*
         * 0 = Main menu, 1 = Settings, 2 = Loading, 3 = Battle, 4 = Build, 
		 * 5 = Tutorial, 6 = LogIn, 7 = Register, 8 = End
         */
		//Create sounds/music
		Sound cannon = Gdx.audio.newSound(Gdx.files.internal("audio/cannon.wav"));
		MainGame.sounds.add(cannon);
		Sound explosion = Gdx.audio.newSound(Gdx.files.internal("audio/explosion.wav"));
		MainGame.sounds.add(explosion); 
		Music menuMusic = Gdx.audio.newMusic(Gdx.files.internal("audio/Menu.wav"));
		menuMusic.setVolume(.75f);
		MainGame.music.add(menuMusic);
		Music battleMusic = Gdx.audio.newMusic(Gdx.files.internal("audio/Battle.wav"));
		battleMusic.setVolume(.75f);
		MainGame.music.add(battleMusic);
        Sound win = Gdx.audio.newSound(Gdx.files.internal("audio/Winsound.wav"));
        MainGame.sounds.add(win);
        Sound lose = Gdx.audio.newSound(Gdx.files.internal("audio/Losesound.wav"));
        MainGame.sounds.add(lose);
		Sound upgrade = Gdx.audio.newSound(Gdx.files.internal("audio/Upgrade.wav"));
		MainGame.sounds.add(upgrade);
		/*Sounds
		 * 0 = cannon, 1 = explosion, 2 = win, 3 = lose, 4 = upgrade,
		 */
		/*Music
		 * 0 = Menu, 1 = Battle
		 */


		//Set startscreen to active screen and play menu music
		// Check if user is logged in

		if(EventManager.getInstance().isSignedIn()) {
			EventManager.getInstance().getUserData();
            EventManager.getInstance().changeScreen(0); // 0 for main menu
        } else {
            EventManager.getInstance().changeScreen(6); // 6 for login
			//EventManager.getInstance().playMusic(MainGame.music.get(0));
        }

	}


	@Override
	public void dispose(){
		for(Sound s : sounds){
			s.dispose();
		}
		for(Music m : music){
			m.dispose();
		}
	}
}
