package com.core.apis;

import com.core.objects.Match;
import java.util.Map;

/**
 * HOW TO DATABASE:
 *
 * - Koble til collection/tabellen: FirebaseHolder.getDatabase().getCollection("matches");
 *      + Tenker at denne initsialiseres når en kamp starter
 *      + Denne funksjonen "abonnerer" samtidig på denne collection
 *
 * - Skrive til collection/tabellen: FirebaseHolder.getDatabase().write(OBJECT)
 *      + Denne kjøres når spiller har skutt
 *      + Motstander sin FirebaseHolder vil da bli trigget
 */

public interface FirebaseServices {
    public void onRegisterButtonClicked(String email, String password);
    public void onLogInButtonClicked(String email, String password);
    public void onLogOutButtonClicked();
    public void onPlayButtonClicked();
    public void onMatchFound();
    public boolean isSignedIn();
    public void onMatchCancel();
    public void onUpdateUserData(Map userData);
    public void onGetUserData();
    public void onUserDelete();
    public void onUpdateMatch(Match match);
}
